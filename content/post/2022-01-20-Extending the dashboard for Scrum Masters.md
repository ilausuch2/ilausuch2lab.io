---
title: Extending the dashboard for Scrum Masters
date: 2022-01-20T09:00:00+02:00
tags: ["agile", "SM", "Scrum master"]
type: post
---

When we finished the initial version of the Scrum Master Dashboard ([A dashboard for Scrum Masters](https://ilausuch2.gitlab.io/a-dashboard-for-scrum-masters/)) we realised
that we need an other tool to analyce our througthput. For this reason I extended the dashboard with a new pair of tables: Incoming vs Outcoming in 30 days, and also in 7 days.

<!--more-->

The motivation for this extension was to understand how and why our backlogs were growing or shrinking. 
If we know how many incoming tasks and outomings do we have during a period of time we could anticipate what is going to happen with our backlog.

We add four columns:
- Incomming tasks: Tasks that has being created during last 30/7 days
- Outcomming tasks: Tasks that we resolved during last 30/7 days
- Backlog length: Current backlog length
- Status: A red alert if the incomming are greater that the outcomming tasks

You can visit our dashboard, it is publish now. The two tables are at the end.
Thank you for reading.

[QE-C dashboard](https://github.com/BillAnastasiadis/qe-c-backlog-assistant)
