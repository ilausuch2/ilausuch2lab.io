---
title: Easy way to use React in a single page
date: 2021-01-20T09:00:00+02:00
tags: ["howto", "tutorial", "javascript", "react"]
type: post
---

On 20 of January of 2021 I prepared a workshop for my team colleges at SUSE about React JS. 
The objective was to introduce React JS and how to use this in single page webs. 
We were NOT using in these examples node JS but a simple HTML that any browser could open and execute.

For thas workshop I prepared a presentation and some examples

- [Presentation](https://hackmd.io/@il-suse/B1o8hKV1_)
- [Examples](https://github.com/ilausuch/training_sessions/tree/main/react)
