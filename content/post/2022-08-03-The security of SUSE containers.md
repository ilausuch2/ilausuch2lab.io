---
title: The security of SUSE containers
date: 2022-08-03T09:00:00+02:00
tags: ["docker", "podman", "containers", "bci"]
type: post
---

I was really interested to confirm what we say on [SUSE bci webpage](https://www.suse.com/products/base-container-images/) as the first
point to use our BCI container images: "Strong security and compliance". This are my results.

<!--more-->

I started using [trivy](https://github.com/aquasecurity/trivy) as a tool to check the security and these are the results. To help
me I created a [containerized trivy image](https://github.com/ilausuch/containerized-trivy)

## Testing some of SUSE images

### opensuse/leap:latest

```
opensuse/leap (opensuse.leap 15.4)
==================================
Total: 0 (UNKNOWN: 0, LOW: 0, MEDIUM: 0, HIGH: 0, CRITICAL: 0)
```

### bci-base:latest
```
registry.suse.com/bci/bci-base:latest (suse linux enterprise server 15.4)
=========================================================================
Total: 0 (UNKNOWN: 0, LOW: 0, MEDIUM: 0, HIGH: 0, CRITICAL: 0)
```

### bci-python:3.10

```
registry.suse.com/bci/nodejs:latest (suse linux enterprise server 15.4)
=======================================================================
Total: 0 (UNKNOWN: 0, LOW: 0, MEDIUM: 0, HIGH: 0, CRITICAL: 0)
```

### bci-nodejs:latest

```
registry.suse.com/bci/python:3.10 (suse linux enterprise server 15.4)
=====================================================================
Total: 0 (UNKNOWN: 0, LOW: 0, MEDIUM: 0, HIGH: 0, CRITICAL: 0)
```

## Testing other images

Also I wanted to put in contrast SUSE images with other common ones

### ubuntu:latest

```
ubuntu:latest (ubuntu 22.04)
============================
Total: 17 (UNKNOWN: 0, LOW: 15, MEDIUM: 2, HIGH: 0, CRITICAL: 0)
```

### alpine:latest

```
alpine:latest (alpine 3.16.1)
=============================
Total: 0 (UNKNOWN: 0, LOW: 0, MEDIUM: 0, HIGH: 0, CRITICAL: 0)
```

## Conclusions
 
I think the quality of Suse container images is really good, and I would promote them for a production environment.