---
title: A dashboard for Scrum Masters
date: 2021-12-01T09:00:00+02:00
tags: ["agile", "SM", "Scrum master"]
type: post
---

When I entered the team "Quality Engineering in Containers and Public Cloud" I was very excited because I had the opportunity to be a Scrum Master of a small team but with 6 projects in parallel, each one of them with a separate backlog. The process of reviewing all the tickets to find problems to solve from the SM perspective was tedious. But one day I found an interesting github project from Vasileios Anastasiadis. It was a dashboard for the QA-Tools team. I contacted him and he was super interested in helping me. This is how we finally designed the dashboard for QE-C team

<!--more-->

In our team we have 6 projects (Containers, JeOS, SLE micro, WSL, Public Cloud and Wicked). Each one has its own backlog.

So we created several tables to cover different topics. Each element of the table is a progress query and we determine if this is in good health or in bad shape depending on the number of results. If the number of results are over a threshold then we understand that this is a problem.

Note: all the threshold limits are based on the SLO or are accorded by the team

Now I am describing the parts of this dashboard in order of creation

First we created the section Abnormalities that includes:
* In Progress But Not Assigned: All the tickets that are in progress but are not assigned to anyone. This case was a problem because as a SM if you have to ask who is working on that to resolve blocks or other issues you have to ask everyone.
* Assigned but not started: This is a soft case, when someone gets some ticket but it didn't change the status. This mate has reserved the ticket for the future? Or he/she is working on that but didn't change the status? Additional we can detect, that is not the case, that someone is picking up too much tickets without start to work on them with the problems that this implies
* New tickets: we want that all the tickets are workable. So this alert allows us to determine that we need a refinement meeting to convert them to workable.
* Coordination without subtasks: In the team, we understand the coordination tickets as epic tickets. So why should it be an epic without tasks? Means that we need a meeting to establish at least the first step to work on that epic

Then we created the sections to cover problems of forgotten tickets. These sections are:
* No Changes in 15 Days: A ticket in progress didn't have any update for more than 15 days. These are critical, because it means that maybe the state is not correct, or the solution is stuck. A standard procedure is to talk with the assignee of this task
* No Changes in 15 Days (Blocked and Feedback): These tickets were waiting for some event that never happens. So the mission here is to know how to unblock that.

Next is the "Pickup time alerts". This is directly associated with the SLO. We divided that depending on the priority: Urgent, High and Normal. This will be in red if some of the tickets for each priority exceeded the time to work with.

Next is the "Backlog lengths by priority". We decided that we don't want more than 1 ticket for the urgent status, 3 for the high status and 20 for the normal status. This helps us to detect problems and move the effort of the team to cover the problematic areas.

And finally the Epics. This section gives us an overview of all the projects, and shows us the progress of every epic.

Hope this is useful to you. 
Thank you for reading.

[QE-C dashboard](https://github.com/BillAnastasiadis/qe-c-backlog-assistant)
