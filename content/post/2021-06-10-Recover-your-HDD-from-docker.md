---
title: Docker is consuming your hard disk?
date: 2021-06-10T09:00:00+02:00
tags: ["howto", "tutorial", "docker"]
type: post
---

If you're a heavy user of docker you'll for sure have a lot of trash in your `/var/lib/docker`

Check for your self:
```
sudo du -sh /var/lib/docker
```

To clean it, follow these simple steps (DISCLAIMER, This will stop and remove all the current containers):

1. Remove the containers (and stopped too)

```
docker ps -a -q | xargs docker rm -f
```

2. Remove all downloaded and build images

```
docker image ls -q |  xargs docker image rm -f
```

3. Remove all volumes 

```
docker volume ls -q | xargs docker volume rm -f
```