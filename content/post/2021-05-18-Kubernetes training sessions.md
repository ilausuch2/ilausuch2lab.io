---
title: Kubernetes & Rancher training sessions
date: 2021-05-18T09:00:00+02:00
tags: ["howto", "tutorial", "kubernetes", "Rancher"]
type: post
---

Some months ago I created a small workshop about kubernetes and Rancher for the my QA SUSE team. Now I would like to share this presentation.

### Kubernetes
{{< youtube hxsufWLQ-BM >}}

### Rancher
{{< youtube 8ZyIggArsyE >}}
