---
title: A openQA container for openQA users
subtitle: Using openQA command line tools without install anything
date: 2021-05-07T09:00:00+02:00
tags: ["howto", "openqa", "containers"]
type: post
---

openQA provides different scripts and a CLI command line application to interact with it. For instance:
- cloning a job with clone_job
- cloning a job from using a test from a git repository
- Accessing to the API using the openqa-cli

But it is necessary to install openqa-devel packages to use this.

The proposal I share here is to use the power of containers to use these scripts without installing anything.

<!--more-->
### Requisites

- [Docker](https://docs.docker.com/engine/install/)
- [Docker compose](https://docs.docker.com/compose/install/)

### How to start

1. First download or clone the git repository https://github.com/ilausuch/openqa_container_console
2. Clone client.conf.template as client.conf and configure all the openQA services you need with a valid URL, key and secret.
3. Build your container ```docker-compose build```
4. Run the container ```docker-compose run -d```

### How to use

You can execute the scripts inside the container, for instance running this API call

```
docker-compose exec console openqa-cli api --host openqa.opensuse.org /jobs limit=1000 latest=1 state="done"
```

But also, in the repository are provided two scripts:
-  clone-job.sh
-  openqa-clone-custom-git.sh

#### Usage of clone-job.sh

```./clone-job.sh <HOST> <JOB ID> <VARIABLE>```

Where:
- host: is the openQA host (e.g. openqa.opensuse.org)
- job id: is the job id to clone (e.g. for https://openqa.opensuse.org/tests/1729306 we write 1729306)
- variable: this is optional but allow to add one variable (e.g. CONTAINERS=1)

#### Usage of openqa-clone-custom-git.sh

```openqa-clone-custom-git-opensuse.sh <HOST> <PR> <Old job> <variable>```

Where:
- host: is the openQA host (e.g. openqa.opensuse.org)
- pr: is the PR to be used for the test
- old job: is the job based to this test (e.g. for https://openqa.opensuse.org/tests/1729306 we write 1729306)
- variable: this is optional but allow to add one variable (e.g. CONTAINERS=1)

### References

- [git repository](https://github.com/ilausuch/openqa_container_console)
- [Cris' post about openqa-cli](https://kalikiana.gitlab.io/post/2021-04-27-working-with-openqa-via-the-command-line/)