---
title: A quick to deploy openQA container for developers
date: 2021-06-11T09:00:00+02:00
tags: ["openqa", "helper", "docker"]
type: post
---

There are different ways to deploy openQA that you can follow [here](https://github.com/os-autoinst/openQA/blob/master/docs/Installing.asciidoc) but usually this deployment is good for testing but for developers there is a better option described in this [Kalikiana's post](https://kalikiana.gitlab.io/post/2021-02-16-get-started-with-openqa-development/)

<!--more-->

So, following the Kalikiana's steps I created a container that do all your job for you and also in a safe container environment.

In the github repo [openqa_developers_container](https://github.com/ilausuch/openqa_developers_container) you will find the instructions to run this. This is a initial commit where you can deploy web UI without any worker yet, but allows you to:

- Play with openQA UI in a isolated environment
- Test your openQA source code adding your branches
- Populate the database with real data from others DB